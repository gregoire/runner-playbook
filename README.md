# GitLab runner playbook

Installs and setup gitlab-runner on debian.

```
ansible-playbook --ask-vault-pass main.yaml -i hosts.yaml
```
